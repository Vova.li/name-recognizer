# Name recognizer

recognize names from different countries based on rules and dictionaries

Names from different countries collected in folders

da - Danish  
de - German  
en - English  
es - Spanish  
fr - French  
he - Hebrew  
it - Italian  
nl - Dutch  
pt-br - Portuguese  
ru - Russian  
tr - Turkish  

# Algorithm

В программу вводит две таблицы, список частоты ФИО и список частоты слов.  
1-если сушествует в обе таблицы  
(Name Popularity) / (Name Popularity + Words Popularity)  
2-если только в таблице имени  
Name Popularity  
3-если только в таблице слова  
-Words Popularity  

Если полученные результаты выше какой-нибудь процент, значит он является ФИО.